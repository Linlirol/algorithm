#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "sort.h"
#include <vector>

TEST(bubble_sort, arr1_default) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::bubble_sort(vec.begin(), vec.end());

    ASSERT_THAT(vec, testing::ElementsAre(1, 2, 3, 4, 5, 6, 10, 12));
}

TEST(bubble_sort, arr1_greater) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::bubble_sort(vec.begin(), vec.end(), std::greater<>());

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(bubble_sort, arr1_lambda) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::bubble_sort(vec.begin(), vec.end(), [](const auto& a, const auto& b){ return a > b; });

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(insertion_sort, arr1_default) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::insertion_sort(vec.begin(), vec.end());

    ASSERT_THAT(vec, testing::ElementsAre(1, 2, 3, 4, 5, 6, 10, 12));
}


TEST(insertion_sort, arr1_greater) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::insertion_sort(vec.begin(), vec.end(), std::greater<>());

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(insertion_sort, arr1_lambda) {
    std::vector<int> vec = {0, 3, 2, 10, 12, 1, 5, 6};
    sorting::insertion_sort(vec.begin(), vec.end(), [](const auto& a, const auto& b){ return a > b; });

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 3, 2, 1, 0));
}


TEST(selection_sort, arr1_default) {
    std::vector<int> vec = {0, 3, 2, 10, 12, 1, 5, 6};
    sorting::selection_sort(vec.begin(), vec.end());

    ASSERT_THAT(vec, testing::ElementsAre(0, 1, 2, 3, 5, 6, 10, 12));
}


TEST(selection_sort, arr1_greater) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::selection_sort(vec.begin(), vec.end(), std::greater<>());

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(selection_sort, arr1_lambda) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::selection_sort(vec.begin(), vec.end(), [](const auto& a, const auto& b){ return a > b; });

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(quick_sort, arr1_default) {
    std::vector<int> vec = {0, 3, 2, 10, 12, 1, 5, 6};
    sorting::quicksort(vec.begin(), vec.end());

    ASSERT_THAT(vec, testing::ElementsAre(0, 1, 2, 3, 5, 6, 10, 12));
}


TEST(quick_sort, arr1_greater) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::quicksort(vec.begin(), vec.end(), std::greater<>());

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(quick_sort, arr1_lambda) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::quicksort(vec.begin(), vec.end(), [](const auto& a, const auto& b){ return a > b; });

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(partition, arr1) {
    std::vector<int> vec = {7, 3, 4, 9, 12, 1, 2, 18, 3};
    sorting::tools::partition(vec.begin(), 0, vec.size() - 1);

    ASSERT_THAT(vec, testing::ElementsAre(1, 3, 4, 3, 2, 7, 12, 18, 9));
}

TEST(partition, border1) {
    std::vector<int> vec = {1};
    auto p = sorting::tools::partition(vec.begin(), 0, vec.size()-1);


    ASSERT_THAT(vec, testing::ElementsAre(1));
}


TEST(partition, greater) {
    std::vector<int> vec = {4, 1, 5};
    auto p = sorting::tools::partition(vec.begin(), 0, vec.size()-1, std::greater<>());


    ASSERT_THAT(vec, testing::ElementsAre(5, 4, 1));
}


TEST(partition, arr2) {
    std::vector<int> vec = {5, 7, 6, 1, 2};
    auto p = sorting::tools::partition(vec.begin(), 0, vec.size()-1);


    ASSERT_THAT(vec, testing::ElementsAre(1, 2, 5, 6, 7));
}

TEST(merge_sort, arr1_default) {
    std::vector<int> vec = {0, 3, 2, 10, 12, 1, 5, 6};
    sorting::merge_sort(vec);

    ASSERT_THAT(vec, testing::ElementsAre(0, 1, 2, 3, 5, 6, 10, 12));
}


TEST(merge_sort, arr1_greater) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::merge_sort(vec, std::greater<>());

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(merge_sort, arr1_lambda) {
    std::vector<int> vec = {4, 3, 2, 10, 12, 1, 5, 6};
    sorting::merge_sort(vec, [](const auto& a, const auto& b){ return a > b; });

    ASSERT_THAT(vec, testing::ElementsAre(12, 10, 6, 5, 4, 3, 2, 1));
}

TEST(merge_func, ex1) {
    std::vector<int> vec1 = {0, 1, 2};
    std::vector<int> vec2 = {3, 4, 5};
    std::vector<int> vec = sorting::tools::merge_sorted(vec1, vec2);

    ASSERT_THAT(vec, testing::ElementsAre(0, 1, 2, 3, 4, 5));

}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	::testing::InitGoogleMock(&argc, argv);

	return RUN_ALL_TESTS();
}
