# Algorithm library
## Sort
sort.h includes namespace sorting with:
* bubble sort
* insertion sort
* selection sort
* merge sort
* quicksort


## Requirements
* C++17 compiler
* cmake 3.20+
* gtest, gmock

## Building and Testing
Building project from source:

```
git clone https://gitlab.com/Linlirol/algorithm.git && cd algorithm/
mkdir build && cd build
cmake ../
make
```

Library includes tests for all methods:
```
cd tests
./run_test_[name]
```

---
[Google Test](https://github.com/google/googletest)
