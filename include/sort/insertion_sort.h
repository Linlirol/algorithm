#pragma once

#include <algorithm>
#include <iterator>

namespace sorting {

// Bidirectional Iterator required
template <typename Iterator, typename Comparator = std::less<>>
void insertion_sort(Iterator begin, Iterator end, Comparator compare = Comparator{}) {
    for (auto it = std::next(begin); it != end; ++it) {
        auto pos = it;
        for (auto s_it = begin; s_it != it; ++s_it) { // searching for element before *it: *pos < *it
            if (compare(*it, *s_it)) {
                pos = s_it;
                break;
            }
        }
        for (auto s_it = it; s_it != pos; --s_it)
            std::iter_swap(std::prev(s_it), s_it);
    }
}

} // namespace sorting
