#pragma once

#include <algorithm>
#include <iterator>
#include <vector>
#include <cassert>
#include <iostream>

namespace sorting {
    namespace tools {

    template <typename T, typename Comparator = std::less<>>
    std::vector<T> merge_sorted(const std::vector<T> &arr1, const std::vector<T> &arr2, Comparator compare = Comparator{}) {
        std::size_t size1 = arr1.size();
        std::size_t size2 = arr2.size();
        std::size_t i = 0, j = 0;
        std::vector<T> merged(size1 + size2);

        for ( ; i < size1 && j < size2; ) {
            if (compare(arr1[i], arr2[j])) {
                merged[i + j] = arr1[i];
                ++i;
                continue;
            }
            merged[i + j] = arr2[j];
            ++j;
        }

        for ( ; i < size1; ++i) {
            merged[i + j] = arr1[i];
        }
        for ( ; j < size2; ++j) {
            merged[i + j] = arr2[j];
        }
        return merged;
    }

    template <typename T, typename Comparator = std::less<>>
    std::vector<T> merge_iter(std::vector<T> &arr, Comparator compare = Comparator{}) {
        if (arr.size() == 1)
            return arr;
        std::vector<T> arr1(arr.begin(), arr.begin() + (arr.size()-1) / 2 + 1);
        std::vector<T> arr2(arr.begin() + (arr.size()-1) / 2 + 1, arr.end());
        return merge_sorted(merge_iter(arr1, compare), merge_iter(arr2, compare), compare);
    }

    } // namespace sorting::tools


// Only for std::vector
template <typename T, typename Comparator = std::less<>>
void merge_sort(std::vector<T> &arr, Comparator compare = Comparator{}) {
    arr = tools::merge_iter(arr, compare);
}

} // namespace sorting
