#pragma once

#include <algorithm>
#include <iterator>
#include <vector>
#include <cassert>

namespace sorting {
    namespace tools {
    template <typename Iterator, typename Comparator = std::less<>>
    std::size_t partition(Iterator begin, std::size_t low, std::size_t high, Comparator compare = Comparator{}) {
        std::size_t low_saved = low;
        // pivot element can be choosed the other way!
        auto key = *(begin + low);
        ++low;

        while (low < high) {
            if (compare( *(begin + low), key)) {
                ++low;
                continue;
            }

            if (compare(key, *(begin + high))) {
                --high;
                continue;
            }

            std::swap(*(begin + low), *(begin + high));
            ++low;
            --high;
        }

        if ( compare(*(begin + low), key) ) {
            std::swap(*(begin + low_saved), *(begin + low));
            return low;
        } else {
            std::swap(*(begin + low_saved), *((begin + low) - 1));
            return low-1;
        }

    }

    template <typename Iterator, typename Comparator = std::less<>>
    void qsort_iter(Iterator begin, std::size_t low, std::size_t high, Comparator compare = Comparator{}) {
        if (low >= high) return;
        std::size_t pi = partition(begin, low, high, compare);
        if (pi > low)
            qsort_iter(begin, low, pi - 1, compare);
        qsort_iter(begin, pi + 1, high, compare);
    }

    } // namespace sorting::tools


// Random-Access Iterator required
template <typename Iterator, typename Comparator = std::less<>>
void quicksort(Iterator begin, Iterator end, Comparator compare = Comparator{}) {
    tools::qsort_iter(begin, 0, (end - begin) - 1, compare);
}

} // namespace sorting
