#pragma once

#include <algorithm>
#include <iterator>

namespace sorting {

// Forward Iterator required
template <typename Iterator, typename Comparator = std::less<>>
void selection_sort(Iterator begin, Iterator end, Comparator compare = Comparator{}) {
    for (auto it = begin; it != end; ++it) {
        auto least = it;
        for (auto jt = it; jt != end; ++jt)
            if (compare(*jt, *least)) least = jt;
        // if (least != it) ?
        std::iter_swap(least, it);
    }
}

} // namespace sorting
