#pragma once

#include <algorithm>
#include <iterator>

namespace sorting {

// Random-Access Iterator required
template <typename Iterator, typename Comparator = std::less<>>
void bubble_sort(Iterator begin, Iterator end, Comparator compare = Comparator{}) {
    for (auto it = begin; it != end; ++it) {
        std::size_t len = it - begin;
        for (auto jt = begin; jt != end - 1 - len ; ++jt)
            if (compare(*(jt+1), *jt))
                std::iter_swap(jt, (jt+1));
    }
}

} // namespace sorting
