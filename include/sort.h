#pragma once

#include "sort/bubble_sort.h"
#include "sort/insertion_sort.h"
#include "sort/selection_sort.h"
#include "sort/quicksort.h"
#include "sort/merge_sort.h"
